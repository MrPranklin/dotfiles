set nocompatible              " be improved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

Plugin 'ddollar/nerdcommenter'          " easy comments
Plugin 'scrooloose/nerdtree'            " file tree
Plugin 'Xuyuanp/nerdtree-git-plugin'    " Git integration for NERDtree
Plugin 'garbas/vim-snipmate'            " snippets
Plugin 'marcweber/vim-addon-mw-utils'   " ^ dependency
Plugin 'tomtom/tlib_vim'                " ^ dependency
Plugin 'honza/vim-snippets'             " ^ dependency
Plugin 'vim-ruby/vim-ruby'              " Ruby plugin
Plugin 'tpope/vim-endwise'              " adds end after if etc.
Plugin 'tpope/vim-rails'                " Rails plugin
Plugin 'tpope/vim-surround'             " surround objects with brackets
Plugin 'sheerun/vim-polyglot'           " syntax and indentation
Plugin 'slim-template/vim-slim'         " slim syntax highlighting
Plugin 'tpope/vim-fugitive'             " Git inegration
Plugin 'airblade/vim-gitgutter'         " shows Git diff in gutter (sign column)
Plugin 'jiangmiao/auto-pairs'           " autoclose brackets
Plugin 'vim-airline/vim-airline'        " better status bar
Plugin 'mileszs/ack.vim'                " search
Plugin 'euclio/vim-markdown-composer'   " Markdown
Plugin 'Chiel92/vim-autoformat'         " formatting
"Plugin 'nelstrom/vim-markdown-folding'  " Markdown folding
"Plugin 'vim-utils/vim-ruby-fold'        " Ruby folding
Plugin 'tpope/vim-bundler'              " Ruby Bundler for Vim
Plugin 'luochen1990/rainbow'            " colored brakcets
Plugin 'nightsense/snow'                " snow color theme

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

source ~/.vim/mappings.vim              " key mappings
source ~/.vim/settings.vim              " vim settings
