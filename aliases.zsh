# Some aliase

alias zshconfig="$EDITOR ~/.zshrc"
alias ohmyzsh="$EDITOR ~/.oh-my-zsh"
alias vimrc="$EDITOR ~/.vimrc"
alias rbh="cd ~/Desktop/ruby-homework"
alias oop="cd ~/Desktop/FER-OOP"
alias desktop="cd ~/Desktop"
alias rs="redshift"
alias jf="cd ~/Desktop/jobfair-fer-unizg-hr-2018"
alias dotfiles="cd ~/Desktop/dotfiles"
alias commit="git commit -m"
alias undo="git reset HEAD~"
