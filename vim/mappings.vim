" toggle file tree with Ctrl+n
map <C-n> :NERDTreeToggle<CR>

" format everyting with F3
map <F3> :Autoformat<CR>

" Open and close tabs
map <leader>tn :tabe<CR>
map <leader>tc :tabclose<CR>

" Split view horizontally or vertically
map <leader>sv :vsplit<cr>
map <leader>sh :split<cr>

" close search
noremap <leader>/ :set hlsearch!<CR>
