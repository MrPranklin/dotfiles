" enable syntax highlighting
syntax on

" set encoding
set termencoding=utf-8

" turn off swap files
set noswapfile

" autoreload files when they change
set autoread

" show line numbers
set number

" Set tab to 2 spaces, disable wrapping, tweaks
set shiftwidth=2
set nowrap
set tabstop=2
set expandtab
set smarttab

" make backspace behave sane
set backspace=2

" Show trailing and preceeding whitespace, show tabs
set list
set listchars=""
set listchars=tab:\|\
set listchars+=trail:.
set listchars+=extends:>
set listchars+=precedes:<

" display all possible command results with tab completion
set wildmenu
set wildmode=list:full,full

" search tweaks
set hlsearch
set incsearch
set ignorecase
set smartcase

" ask to save changes when exiting
set confirm

" enable mouse in all modes
set mouse=a

" never hide the mouse
set nomousehide

" always show the status bar
set laststatus=2

" position splits logically
set splitbelow
set splitright

" NERDTree config
let NERDTreeShowHidden=1

" Git NERDTree config
let g:NERDTreeShowIgnoredStatus = 1

" enable close button in the upper right corner for tabs
let g:tablineclosebutton=1

" astyle formatting default
let b:formatdef_custom_c= '"astyle --stlyle=java"'
let b:formatters_c = ['custom_c']

" autoenable colored brackets
let g:rainbow_active = 1

" colorschemes
"set background=dark
"set background=light
"colorscheme snow
colorscheme molokai
