# Spaceship theme config

# For Vi mode
alias vienable="spaceship_vi_mode_enable"
alias vidisable="spaceship_vi_mode_disable"

# Exit code
export SPACESHIP_EXIT_CODE_SHOW=true
export SPACESHIP_EXIT_CODE_SYMBOL=""

# Timestamp
export SPACESHIP_TIME_SHOW=true
export SPACESHIP_TIME_COLOR=green

# Execution time
export SPACESHIP_EXEC_TIME_ELAPSED=1

# Battery
export SPACESHIP_BATTERY_THRESHOLD=70

# Git
export SPACESHIP_GIT_BRANCH_COLOR=green

# Prompt order on the right
SPACESHIP_RPROMPT_ORDER=(
time
exec_time
battery
)

# Prompt order on the left
SPACESHIP_PROMPT_ORDER=(
user
dir
host
git
package
node
ruby
pyenv
dotnet
line_sep
vi_mode
jobs
exit_code
char
)
