Repository of dotfiles I currently find important.

- `Vim` 
    - `/vim` - mappings and settings for Vim
    - `vimrc`
- `Git`
    - `gitconfig`
    - `gitignore_global`
- `zsh`
    - `aliases.zsh` - aliases for z shell
    - `spaceship.zsh` - spaceship theme config
- `bash` 
    - `bashrc`
    - `bash_profile`
- `ideavimrc` - for Vim plugin for IntelliJ IDEA
