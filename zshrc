# In case zsh doesn't know some commands
# from when I was just a regular kid
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# For Vi mode
export RPS1="%{$reset_color%}"
export KEYTIMEOUT=1

# Path to oh-my-zsh installation
export ZSH="/home/filip/.oh-my-zsh"

# Themes
#source  ~/powerlevel9k/powerlevel9k.zsh-theme
#ZSH_THEME="robbyrussell"
#ZSH_THEME="agnoster"
#ZSH_THEME="avit"
#ZSH_THEME="bullet-train"
ZSH_THEME="spaceship"
# Spaceship theme config
source ~/.spaceship.zsh

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# History timestamp format
HIST_STAMPS="dd.mm.yyyy"

# Enabled plugins
plugins=(
git
bundler
dotenv
rake
rbenv
ruby
gem
npm
pip
python
sudo              # ESC x2 puts sudo in front
chucknorris       # print a Chuck Norris joke
#command-not-found # tell how to install missing command
zsh-syntax-highlighting
composer          # autocomplete
common-aliases    # collection of zsh aliases
extract           # extract archive
)

source $ZSH/oh-my-zsh.sh

#----------------USER CONFIGURATION----------------

# Encoding
export LANG=en_US.UTF-8

# Set Vim as the default editor
export EDITOR='vim'
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR

# Aliases
source ~/.aliases.zsh

# VTE config
if [ $TILIX_ID  ] || [ $VTE_VERSION  ]; then
  source /etc/profile.d/vte.sh
fi
